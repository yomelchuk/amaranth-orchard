# amaranth-orchard

Curated Amaranth SoC cores

This repository contains existing open source SoC combined with wrappers and glue to enable Amaranth support.

It also contains components from lambdasoc to enable Control&Status Register (CSR) support for amaranth-soc cores.

All existing code retains the copyright and license of its original developers; see original files for further details.



